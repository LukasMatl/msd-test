import {gt, gte, isNil, times, equals} from 'ramda';

const getTheLargestStringLength = (fatherSequence,testSequence) =>
    gte(fatherSequence.length,testSequence.length)? fatherSequence.length: testSequence.length;

export const getMutationCount = (fatherSequence, testSequence) => {
    let count = 0;
    if (!isNil(fatherSequence) && !isNil(testSequence)) {
        const length = getTheLargestStringLength(fatherSequence, testSequence);
        times((index) => {
            if (!isEqual(fatherSequence,testSequence, index) && (
                isDeletion(fatherSequence,testSequence, index) ||
                isInsertion(fatherSequence,testSequence, index))
            ) {
              count++;
            }
        }, length);
    } else {
      count = -1;
    }
    return count;
}

const isDeletion = (fatherSequence,testSequence, index) => equals(fatherSequence[index], testSequence[index+1]);
const isInsertion = (fatherSequence,testSequence, index) => !equals(fatherSequence[index], testSequence[index]) && equals(fatherSequence[index], testSequence[index+1]);
const isEqual = (fatherSequence,testSequence, index) => equals(fatherSequence[index],testSequence[index]);