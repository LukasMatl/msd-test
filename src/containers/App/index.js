import React, { Component } from 'react';
import '../../sass/app.scss';
import {getMutationCount} from "../../util";

class App extends Component {
  render() {
    return (
        <div className="App">
          <div className="content">
            <ul>
              <li>"" -> "" = {getMutationCount("","")}</li>
              <li>"AAA" -> "AAA" = {getMutationCount("AAA","AAA")}</li>
              <li>"ATA" -> "AAA" = {getMutationCount("ATA","AAA")}</li>
              <li>"AA" -> "AAA" = {getMutationCount("AA","AAA")}</li>
              <li>"AAAA" -> "AAA" = {getMutationCount("AAAA","AAA")}</li>
              <li>"ACTGCTCGTATCGATCGATCAC" -> "ACTCCGTATCGAGCGATCACAG" = {getMutationCount("ACTGCTCGTATCGATCGATCAC","ACTCCGTATCGAGCGATCACAG")}</li>
            </ul>
          </div>
        </div>
    );
  }
}

export default App;
